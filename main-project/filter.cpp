#include "filter.h"
#include <cstring>
#include <iostream>

kurs_subscription** filter(kurs_subscription* array[], int size, bool (*check)(kurs_subscription* element), int& result_size)
{
	kurs_subscription** result = new kurs_subscription * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_book_subscription_by_bank(kurs_subscription* element)
{
	return strcmp(element->bank, "�����������") == 0;
}

bool check_book_subscription_by_val(kurs_subscription* element)
{
	return element->prodaja<2.5;
}
