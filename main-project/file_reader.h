#ifndef FILE_READER_H
#define FILE_READER_H

#include "kurs.h"

void read(const char* file_name, kurs_subscription* array[], int& size);

#endif