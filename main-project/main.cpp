#include <iostream>
#include <iomanip>

using namespace std;

#include "kurs.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(kurs_subscription* subscription)
{

	cout << subscription->bank << " ";
	cout << subscription->pokupka << " ";
	cout << subscription->prodaja << " ";
	cout << subscription->adres << " ";
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "Laboratory work #1. GIT\n";
	cout << "Variant #4. kurs_valute\n";
	cout << "Author: Aleksandr Kurnosenko\n";
	cout << "Group: 14Z\n";
	kurs_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("../data.txt", subscriptions, size);
		for (int i = 0; i < size; i++)
		{
			output(subscriptions[i]);
		}
		bool (*check_function)(kurs_subscription*) = NULL; // check_function -    ,    bool,
														   //        book_subscription*
		cout << "1) ������� ����� ������ �� ���� ���������� ����� ������������ (� ��������). \n";
		cout << "2) ������� ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5.\n";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_book_subscription_by_bank; //       
			cout << "*****        *****\n\n";
			break;
		case 2:
			check_function = check_book_subscription_by_val; //       
			cout << "*****        *****\n\n";
			break;
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			kurs_subscription** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
