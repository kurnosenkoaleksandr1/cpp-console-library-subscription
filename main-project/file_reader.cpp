#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>



void read(const char* file_name, kurs_subscription* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            kurs_subscription* item = new kurs_subscription;
            file >> item->bank;
            file >> item->pokupka;
            file >> item->prodaja;
            file.getline(item->adres, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}