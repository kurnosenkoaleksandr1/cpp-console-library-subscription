#ifndef FILTER_H
#define FILTER_H

#include "kurs.h"

kurs_subscription** filter(kurs_subscription* array[], int size, bool (*check)(kurs_subscription* element), int& result_size);
bool check_book_subscription_by_bank(kurs_subscription* element);
bool check_book_subscription_by_val(kurs_subscription* element);


#endif
